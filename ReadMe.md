Description générale du projet:

    Le projet a pour objectif de reconnaitre plusieurs entités telles que des humains, des chars d'assault, etc avec une bonne fiabilité.
    En utilisant ce programme, il sera possible aux utilisateurs du drone de détecter des entités qu'ils n'auraient peut être pas vu.


Etat du projet: 
    La phase de programmation est terminée. Il ne reste qu'à entrainer le réseau de neurones sur différentes images.


Installation et utilisation:    

    Placez vous dans un dossier et effectuez les 2 commandes suivantes:
        $ git clone https://gitlab-student.centralesupelec.fr/yohann.sanvert/dassault-uav-challenge-centralesupelec.git
        $ cd dassault-uav-challenge-centralesupelec

    Pour installer l'environnement nécessaire à l'utilisation de la bibliothèque, executez la commande suivante:
        python -m pip install -r requirements.txt --user

    Effectuez les téléchargements suivants et les mettre dans model_data.
        https://pjreddie.com/media/files/yolov3.weights
        https://pjreddie.com/media/files/yolov3-tiny.weights
        https://github.com/AlexeyAB/darknet/releases/download/darknet_yolo_v3_optimal/yolov4.weights
        https://github.com/AlexeyAB/darknet/releases/download/darknet_yolo_v4_pre/yolov4-tiny.weights
        

    Pour lancer l'application, il faut placer le terminal dans le dossier source: 
        "dassault-uav-challenge-centralesupelec"
    Ensuite, dans le terminal, écrivez python -m + le chemin vers le fichier utils.py (sans le .py):
        < python -m yolov3.utils > 
    Enfin, pour fermer la fenêtre, appuyez sur "CTRL + C" dans le terminal
    

Technologie utilisée: 
    YOLO V3

Equipe :
    Benayoun Yohan
    Gouiffes Matteo
    Lescure Amaury
    Nicolino Henri-Jean
    Sanvert Yohann
    Scharffe Matthieu
